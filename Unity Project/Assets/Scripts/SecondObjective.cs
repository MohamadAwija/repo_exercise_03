﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class SecondObjective : MonoBehaviour
    {
        [SerializeField]
        // Maximum player health points 
        private int playerHealth = 100;
        public int PlayerHealth
        {
            get { return playerHealth = 100; }
        }

        [SerializeField]
        // Name of item equipped 
        private string playerItem = "Sword";
        public string PlayerItem
        {
            get { return playerItem = "Sword"; }
        }

        [SerializeField]
        // Indicates if player is alive
        private bool isPlayerAlive = true;
        public bool IsPlayerAlive
        {
            set { isPlayerAlive = true; }
        }

        [SerializeField]
        // Player movement speed in meters per second
        private float playerSpeed = 1.4f;
        public float PlayerSpeed
        {
            set { playerSpeed = 1.4f; }
        }
        
    }
}