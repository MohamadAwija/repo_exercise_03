﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class ThirdObjective : MonoBehaviour
    {
        public SampleEntity Player;

        private void Start()
        {
            Player.PlayerName = "Mohamad";
            string Name = Player.PlayerName;
            Debug.Log(Name);

            Player.PlayerAge = 22;
            int Age = Player.PlayerAge;
            Debug.Log(Age);

            Player.PlayerHeight = 190.0f;
            float Height = Player.PlayerHeight;
            Debug.Log(Height);

            Player.CurrentYear = 2019;
            int Year = Player.CurrentYear;
            Debug.Log(Year);

            Player.PlayerFriends = 2;
            int Friends = Player.PlayerFriends;
            Debug.Log(Friends);
        }
    }
}