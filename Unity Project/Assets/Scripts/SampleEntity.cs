﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class SampleEntity : MonoBehaviour
    {
        [SerializeField]
        // The name of the player 
        private string playerName = "Mohamad";
        public string PlayerName
        {
            get { return playerName; }
            set { playerName = "Mohamad"; }
        }

        [SerializeField]
        // The number of years since player birth
        private int playerAge = 22;
        public int PlayerAge
        {
            get { return playerAge; }
            set { playerAge = 22; }
        }

        [SerializeField]
        // Player height in centimeters
        private float playerHeight = 190.0f;
        public float PlayerHeight
        {
            get { return playerHeight; }
            set { playerHeight = 190.0f; }
        }

        [SerializeField]
        // Current year in the player world
        private int currentYear = 2019;
        public int CurrentYear
        {
            get { return currentYear; }
            set { currentYear = 2019; }
        }

        [SerializeField]
        // The number of best friends the player has
        private int playerFriends = 2;
        public int PlayerFriends
        {
            get { return playerFriends; }
            set { playerFriends = 2; }
        }

        // This method can be used for the player's dialogue
        public void startTalking()
        {

        }

        // This method can be used when player starts running
        public void startRunning(float speed)
        {

        }


    }
}
